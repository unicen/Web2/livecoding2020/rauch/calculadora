<?php
    require_once('operaciones.php'); // incluye archivo de operaciones básicas

    // realiza controles de entrada de datos del usaurio
    if (!is_numeric($_REQUEST['valor1']) ||
        !is_numeric($_REQUEST['valor2']) ||
        empty($_REQUEST['operacion']) 
        ) 
    {
        echo('ERROR: Parámetros incorrectos');
        die();
    }

    // obtiene los valores enviados por el usuario
    $operando1 = $_REQUEST['valor1'];
    $operando2 = $_REQUEST['valor2'];
    $operacion = $_REQUEST['operacion'];

    // decide que operación realizar
    switch ($operacion) {
        case 'sumar': {
            $resultado = sumar($operando1, $operando2); 
            break;
        }
        case 'restar': {
            $resultado = restar($operando1, $operando2); 
            break;
        }

        case 'multiplicar': {
            $resultado = multiplicar($operando1, $operando2); 
            break;
        }

        case 'dividir': {
            $resultado = dividir($operando1, $operando2); 
            break;
        }

        default: {
            echo ('Operacion no definida.'); 
            break;
        }

    }

    echo('El resultado es: '.$resultado);

?>